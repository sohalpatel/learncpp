cmake_minimum_required(VERSION 3.1)
# project name
project(app)

set(HEADER_PATH_APP
    "${CMAKE_CURRENT_SOURCE_DIR}/include"
)

set(SRC_APP
    "${CMAKE_CURRENT_SOURCE_DIR}/source/main.c"
)

# hint: we use a object library to resolve weak references (#pragma WEAK)    
add_library( ApplicationProject OBJECT
    ${SRC_APP}
)

target_include_directories( ApplicationProject 
    PUBLIC 
    ${HEADER_PATH_APP} 
)

