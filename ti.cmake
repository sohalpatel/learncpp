#################################################################
# @file   ti.cmake
# @brief  toolchain file for CMake to use TI
# @author sPatel
# 
#################################################################


#################################################################
# set toolchain programs and paths
#################################################################
cmake_minimum_required(VERSION 3.1)
set(TI_COMPILER_ROOT "/c/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.4.LTS")
set( LINK_COMMAND_FILE "${CMAKE_SOURCE_DIR}/HAL/source/sys_link.cmd")
# Targeting an embedded system, no OS.
set( CMAKE_SYSTEM_NAME Generic )
set( CMAKE_SYSTEM_PROCESSOR TMS570LS3137 )

# specify the cross compiler
set( CMAKE_C_COMPILER   "${TI_COMPILER_ROOT}/bin/armcl" )

set( CMAKE_CXX_COMPILER "${CMAKE_C_COMPILER}" )
set( CMAKE_ASM_COMPILER "${CMAKE_C_COMPILER}" )

# skip compiler tests so there is no need to force
# the compiler via CMAKE_FORCE_C_COMPILER
# but CMAKE_C_FLAGS has to specified due to tests
set( CMAKE_ASM_COMPILER_WORKS         0 )
set( CMAKE_C_COMPILER_WORKS           0 )
set( CMAKE_CXX_COMPILER_WORKS         0 )
set( CMAKE_DETERMINE_ASM_ABI_COMPILED 0 )
set( CMAKE_DETERMINE_C_ABI_COMPILED   0 )
set( CMAKE_DETERMINE_CXX_ABI_COMPILED 0 )

# set target environment
set( CMAKE_FIND_ROOT_PATH ${TI_COMPILER_ROOT} )

# search for programs in the build host directories
set( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )

# for libraries and headers in the target directories
set( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
set( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )

message( STATUS "Cross-compiling with the TI ARM 18.1.4 LTS toolchain" )

# include compiler specific headers
find_path( TOOLCHAIN_INCLUDE stdlib.h PATHS "${TI_COMPILER_ROOT}/include" )
if( TOOLCHAIN_INCLUDE )
    include_directories( SYSTEM ${TOOLCHAIN_INCLUDE} )
else()
    message( FATAL_ERROR "Failed to find stdlib.h in ${TI_COMPILER_ROOT}/include" )
endif()

#################################################################
# Specify statically the build type
#################################################################

# Specifies the build type on single-configuration generators
# and can be overwritten by command line option (CACHE)
# -DCMAKE_BUILD_TYPE=Debug
# -DCMAKE_BUILD_TYPE=Release
# -DCMAKE_BUILD_TYPE=RelWithDebInfo
set( CMAKE_BUILD_TYPE Release CACHE STRING "Used Build Type" )
set( BUILD_TARGET Field_AX CACHE STRING "Used Build Target" )

#################################################################
# Set Compiler Flags for TI ARM Compiler
#################################################################

set( CMAKE_C_FLAGS                  "" )
set( CMAKE_CXX_FLAGS                "" )
set( CMAKE_ASM_FLAGS                "" )

set( CMAKE_C_FLAGS_DEBUG            ""  CACHE STRING "c flags, build type Debug"            FORCE )
set( CMAKE_C_FLAGS_RELEASE          ""  CACHE STRING "c flags, build type Release"          FORCE )

set( CMAKE_CXX_FLAGS_DEBUG          ""  CACHE STRING "c++ flags, build type Debug"          FORCE )
set( CMAKE_CXX_FLAGS_RELEASE        ""  CACHE STRING "c++ flags, build type Release"        FORCE )

set( CMAKE_ASM_FLAGS_DEBUG          ""  CACHE STRING "ASM flags, build type Debug"          FORCE )
set( CMAKE_ASM_FLAGS_RELEASE        ""  CACHE STRING "ASM flags, build type Release"        FORCE )

set( CMAKE_EXE_LINKER_FLAGS         ""  CACHE STRING "executable linker flags"              FORCE )


set( COMPILER_FLAGS_NONE
    --symdebug:dwarf
)

# The following compiler flags are copied from CCS (Properties > CCS Build > ARM Compiler).
# NOTE: By reselecting all compiler settings, also default flags like for example 
#       --wchar_t=16 appear in the flag summary and are added here.

set( COMPILER_FLAGS_DEBUG
    -mv7R4
    --code_state=32
    --float_support=VFPv3D16
    --abi=eabi
    -Ooff
    --fp_mode=strict
    --opt_for_speed=1
    -g
    --symdebug:dwarf_version=3
    --c99
    --c++14
    --diag_warning=225
    --diag_wrap=off
    --display_error_number
    --sat_reassoc=off
    --unaligned_access=on
    --enum_type=packed
    --common=on
    --fp_reassoc=off
    --plain_char=unsigned
    --embedded_constants=on
    --wchar_t=16
)

set( COMPILER_FLAGS_RELEASE
    -mv7R4
    --code_state=32
    --float_support=VFPv3D16
    --abi=eabi
    -O4
    --fp_mode=strict
    --opt_for_speed=5
    -g
    --symdebug:dwarf_version=3
    --c99
    --c++14
    --diag_warning=225
    --diag_wrap=off
    --display_error_number
    --sat_reassoc=off
    --unaligned_access=on
    --enum_type=packed
    --common=on
    --fp_reassoc=off
    --plain_char=unsigned
    --embedded_constants=on
    --wchar_t=16
)

# When we break up long strings in CMake we get semicolon
# separated lists, undo this here...
string( REGEX REPLACE ";" " " COMPILER_FLAGS_NONE    "${COMPILER_FLAGS_NONE}"    )
string( REGEX REPLACE ";" " " COMPILER_FLAGS_DEBUG   "${COMPILER_FLAGS_DEBUG}"   )
string( REGEX REPLACE ";" " " COMPILER_FLAGS_RELEASE "${COMPILER_FLAGS_RELEASE}" )

#################################################################
# Set Linker Flags
#################################################################

# The following linker flags are copied from CCS (Properties > CCS Build > ARM Linker).
# NOTE: TI_COMPILER_ROOT variable has to be used for search paths set by -i.

set( LINKER_FLAGS
    --stack_size=0x4000
    --heap_size=0x800
    -i"${TI_COMPILER_ROOT}/lib"
    -i"${TI_COMPILER_ROOT}/include"
    --reread_libs
    --diag_wrap=off
    --display_error_number
    --warn_sections
    --rom_model
    --be32 
)

# When we break up long strings in CMake we get semicolon
# separated lists, undo this here...
string( REGEX REPLACE ";" " " LINKER_FLAGS "${LINKER_FLAGS}" )

# specify if linker is invoked with optimization
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    set( LINKER_EXTRA_FLAGS "" )
else()
    set( LINKER_EXTRA_FLAGS "--opt_level=4 --opt_for_speed=5" )
endif()

#################################################################
# Specify Build Types
#################################################################

# do not write 'normal'-Flags to cache.
# They are only used temporary for compiler tests
set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} ${COMPILER_FLAGS_NONE}" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_C_FLAGS}"     )
set( CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} ${CMAKE_C_FLAGS}"     )

# combine flags to C and C++ target cpu flags, CMAKE_BUILD_TYPE=Debug
set( CMAKE_C_FLAGS_DEBUG   "${CMAKE_C_FLAGS_DEBUG} ${COMPILER_FLAGS_DEBUG}"  CACHE STRING "Debug C-Flags"   FORCE )
set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${CMAKE_C_FLAGS_DEBUG}" CACHE STRING "Debug Cpp-Flags" FORCE )
set( CMAKE_ASM_FLAGS_DEBUG "${CMAKE_ASM_FLAGS_DEBUG} ${CMAKE_C_FLAGS_DEBUG}" CACHE STRING "Debug ASM-Flags" FORCE )

# combine flags to C and C++ target cpu flags, CMAKE_BUILD_TYPE=Release
set( CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} ${COMPILER_FLAGS_RELEASE}"  CACHE STRING "Release C-Flags"   FORCE )
set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${CMAKE_C_FLAGS_RELEASE}" CACHE STRING "Release Cpp-Flags" FORCE )
set( CMAKE_ASM_FLAGS_RELEASE "${CMAKE_ASM_FLAGS_RELEASE} ${CMAKE_C_FLAGS_RELEASE}" CACHE STRING "Release ASM-Flags" FORCE )

# set linker flags in necessary sequence
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LINKER_FLAGS}" CACHE STRING "Linker Flags" FORCE )

set( BUILD_SHARED_LIBS OFF )

#################################################################
# Replace source file extension for object files
# (Why: Like CCS, we want to use *.obj instead of *.cpp.obj / *.c.obj.)
# See http://cmake.3232098.n2.nabble.com/cmake-output-file-naming-td7581261.html
#################################################################

set( CMAKE_C_OUTPUT_EXTENSION_REPLACE ON )
set( CMAKE_CXX_OUTPUT_EXTENSION_REPLACE ON )
set( CMAKE_ASM_OUTPUT_EXTENSION_REPLACE ON )

#################################################################
# Set response file suppression (rsp-file)
# (Why: Workaround to avoid make problems on Windows with newer CMake versions.)
# See https://github.com/kripken/emscripten/issues/2295
#################################################################

set( CMAKE_C_USE_RESPONSE_FILE_FOR_OBJECTS OFF )
set( CMAKE_CXX_USE_RESPONSE_FILE_FOR_OBJECTS OFF )
set( CMAKE_C_USE_RESPONSE_FILE_FOR_INCLUDES OFF )
set( CMAKE_CXX_USE_RESPONSE_FILE_FOR_INCLUDES OFF )

set( CMAKE_C_RESPONSE_FILE_LINK_FLAG "@" )
set( CMAKE_CXX_RESPONSE_FILE_LINK_FLAG "@" )

# the following lib will be included within the linker command file and is therefor necessary
if( NOT EXISTS "/c/ti/Hercules/F021 Flash API/02.01.01/F021_API_CortexR4_BE_V3D16.lib" )
    message( FATAL_ERROR "/c/ti/Hercules/F021 Flash API/02.01.01/F021_API_CortexR4_BE_V3D16.lib not found" )
endif()

if( NOT EXISTS "${TI_COMPILER_ROOT}/lib/rtsv7R4_T_be_v3D16_eabi.lib" )
    message( FATAL_ERROR "${TI_COMPILER_ROOT}/lib/rtsv7R4_T_be_v3D16_eabi.lib not found" )
endif()

# due to dependency of ordering objects and libraries
# it must be ensured that the rts library comes at the end.
# This is done with <LINK_LIBRARIES>
set( LINK_COMMAND_FILE "${CMAKE_SOURCE_DIR}/hal/source/sys_link.cmd" )
set( CMAKE_C_LINK_EXECUTABLE   "${CMAKE_C_COMPILER}   ${LINKER_EXTRA_FLAGS} --run_linker --output_file=<TARGET> --map_file=<TARGET>.map <OBJECTS> ${LINK_COMMAND_FILE} <CMAKE_C_LINK_FLAGS>   <LINK_FLAGS> <LINK_LIBRARIES>")
set( CMAKE_CXX_LINK_EXECUTABLE "${CMAKE_CXX_COMPILER} ${LINKER_EXTRA_FLAGS} --run_linker --output_file=<TARGET> --map_file=<TARGET>.map <OBJECTS> ${LINK_COMMAND_FILE} <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>")
set( CMAKE_ASM_LINK_EXECUTABLE "${CMAKE_ASM_COMPILER} ${LINKER_EXTRA_FLAGS} --run_linker --output_file=<TARGET> --map_file=<TARGET>.map <OBJECTS> ${LINK_COMMAND_FILE} <CMAKE_ASM_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>")

set( TI_INCLUDES
    "${TI_COMPILER_ROOT}/include"
    "/c/ti/Hercules/F021_Flash_API/02.01.01/include"
)

set(TI_EABI
    "${TI_COMPILER_ROOT}/lib/rtsv7R4_T_be_v3D16_eabi.lib"
)